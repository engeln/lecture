# Programming Techniques for Scientific Simulations I ([402-0811-00L](http://www.vorlesungsverzeichnis.ethz.ch/Vorlesungsverzeichnis/lerneinheit.view?lerneinheitId=140944&semkez=2020W&ansicht=KATALOGDATEN&lang=en))

### General information

The course will take place in a **hybrid presence / online** format:

  * **Presence**: Thursday 13:45 - 17:30, [HCI](http://www.mapsearch.ethz.ch/map/mapSearchPre.do?gebaeudeMap=HCI&geschossMap=G&raumMap=3&farbcode=c010&lang=en) [G 3]( http://www.rauminfo.ethz.ch/Rauminfo/grundrissplan.gif?gebaeude=HCI&geschoss=G&raumNr=3&lang=en) [>>](http://www.rauminfo.ethz.ch/Rauminfo/RauminfoPre.do?gebaeude=HCI&geschoss=G&raumNr=3&lang=en)
  * **Online**: Live stream (the Meeting ID was sent in the welcome email on
    Monday, September 14. If you were not enrolled in the course then, please
    send an email to the mailing list)

To comply with the ETHZ safety concept hygiene measures and room ventilation
regulations, the course will proceed in two 90 minutes blocks with an extended
25 minutes break in between.
**All participants** must leave the room during the break and must return to
the same places as before (of course, only if you choose to return!).

We kindly ask **all participants** to comply with the up-to-date latest
Coronavirus COVID-19 measures:

  * <https://ethz.ch/services/en/news-and-events/coronavirus.html>

### Summary

This lecture provides an overview of programming techniques for scientific
simulations.
The focus is on advanced C++ programming techniques and scientific software
libraries.
Based on an overview over the hardware components of PCs and supercomputers,
optimization methods for scientific simulation codes are explained.

### Questions

For questions or remarks we have a mailing list where you can reach us:
<pt1_hs20@sympa.ethz.ch >

## Lecture slides, exercises and solutions

Lecture slides, exercise sheets and solutions will be provided as part of this
git repository.

## Submission

If you want to receive feedback on your exercises, please push your solutions
to your own git repository before **Monday night** of the week after we hand
out the exercise sheet.
Then send a notification / request for correction email (possibly with
questions) to the mailing list.
Advanced users can utilise GitLab issues (make sure to tag all the assistants,
but not the professor, with their @name in the issue description).

Your exercise will then be corrected before the next exercise session.
Make sure to give *maintainer* access to the following people:
@karoger, @engelerp, @ilabarca, @msudwoj, @pollakg and @rworreby.

Of course, working in small groups is allowed (and even encouraged using a
collaborative workflow with `git` and `GitLab`).
However, please make sure that you understand every part of the group's
proposed solution (you will have to e.g., at the exam!).
If several group members submit the exercises, please indicate clearly in the
notification/request for correction email all the group members and indicate
which parts of the solution you would like to have looked at individually
(although we try to run data comparison tools carefully during the correction,
we may miss some individual solution of group members).

## Course confirmation (Testat)

For students needing the confirmation (Testat) for this course, we require
that 70% of the exercises have been solved reasonably (sinnvoll).
The submission deadline is every Wednesday midnight (Zurich time!).

Please announce that you want the confirmation (Testat) for this course
explicitly at the beginning of the semester. Contact us either in person or
through the mailing list.

## Exam information

* For general information, see the performance assessment tab in the course
  catalogue [here](http://www.vorlesungsverzeichnis.ethz.ch/Vorlesungsverzeichnis/lerneinheit.view?semkez=2020W&ansicht=LEISTUNGSKONTROLLE&lerneinheitId=140944&lang=en).

* The exam will have two parts: A written theoretical part, and a programming
  part that you will solve on the exam computers.

* The exam computers will run Fedora Linux, similar to those that you find in
  the computer rooms in the ETH main building.
  The system language of the computers is English.
  A list of the installed software can be found [here](https://www.ethz.ch/services/en/it-services/catalogue/managed-client/computer-rooms.html).

* By default, the keyboards will have the Swiss layout.
  There will be a poll for those who want to get a US keyboard instead.

* Provided on the computers are:
    * The full lecture repository
    * The C++ standard ([draft version](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2012/n3337.pdf))
    * An offline version of http://www.cppreference.com
      (See *Html book* https://en.cppreference.com/w/Cppreference:Archives)  
      We recommend that you try it out before the exam.
      Also note that the search function is absent: use the Index page and the
      search function of the browser.
    * An offline version of [the Python documentation](https://docs.python.org/3.8/index.html)
      (See https://docs.python.org/3.8/download.html)
      We recommend that you try it out before the exam.
    * As needed, offline versions of the documentation for Python libraries.

* This is an open-book exam, which means that you can bring any written
  material (books, notes, printed code, ...).
  However, you may **not** use any digital devices (other than the exam
  computer) during the exam.

* Don't forget to bring your student card (Legi).

